import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/city_select.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';

import 'helpers/connection.dart';
import 'noConnection.dart';

class SplashScreen extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
   return SplashScreenState();
  }

}
class SplashScreenState extends State<SplashScreen>{
  String city;
  bool isConnected;
  bool dialogIsShown;
 var subscription;
  @override
  Widget build(BuildContext context) {
    checkConnection();
    // TODO: implement build
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(color: Color.fromRGBO(120, 0, 1, 1)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:
            Image.asset('images/splash.jpg',fit: BoxFit.contain,height: MediaQuery.of(context).size.height,),


      ),
    );

  }
  checkConnection() async{
    var dio = Dio();
    dio..interceptors.add(RetryInterceptor(
      options: const RetryOptions(
        retries: 10,
        retryInterval: Duration(seconds: 3),
      )
    ));
    Response response =await dio.get('https://www.feresta.ir/wp-json/dokan/v1/stores');
    if(response.statusCode == 200){
      (city != '' && city !=null)?Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MainPage())):Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>SelectCity()));
    }
}
  @override
  void initState()  {
    getCity();
//    Connection.checkConnection(context);
    super.initState();
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




  });


  }

  @override
  void dispose() {
    getCity();
    super.dispose();
    subscription.cancel();

  }
  getCity() async{

    city = await user_shared.getCity();
  }


}