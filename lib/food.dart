import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/data_models/basket_product.dart';
import 'package:ferestaapp/data_models/model_products.dart';
import 'package:ferestaapp/data_models/model_shops.dart';
import 'package:ferestaapp/drawer.dart';
import 'package:ferestaapp/final_cart.dart';
import 'package:ferestaapp/helpers/html_parser.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:ferestaapp/showCart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:persian_numbers/persian_numbers.dart';
import 'package:ferestaapp/helpers/html_parser.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sweetalert/sweetalert.dart';

import 'helpers/connection.dart';
import 'helpers/formatter.dart';
import 'login.dart';

class Food extends StatefulWidget {
  ModelShops modelShops;
  bool fromFood;
  int foodIndex;
  int store_id;
  List<Product> products = List();

  Food(
      {Key key,
      this.title,
      this.modelShops,
      this.fromFood,
      this.foodIndex,
      this.store_id})
      : super(key: key);
  List<int> items = new List();
  List<Product> allItems = List();
  final String title;

  @override
  FoodState createState() => new FoodState(modelShops);
}

class FoodState extends State<Food> with SingleTickerProviderStateMixin {
  bool isInBasket = false;
  ModelShops modelShops;
  List<ModelProducts> itembyCategory = List();
  List<Widget> _widgets = List();
  int selected = 0;
  int count = 0;
  int store_id;
  List<ModelProducts> bottom_items = List();

  List categoryListId = List();
  List finalCategoryListId = List();
  List categoryListName = List();

  FoodState(this.modelShops);

  List finalCategoryListName = List();
  List<ModelProducts> modelProduct;
  int index;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<TextEditingController> _controller = List();
  bool isConnected;
  List<FocusNode> _focus = List();
  TabController _tabController;
  ScrollController scrollController;
  List<Product> list = List();
  List<Product> basket_list = List();
  var product_count = '0';
  var subscription;

  @override
  void initState() {
    super.initState();
    get_products();
//    Connection.checkConnection(context);
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      // Got a new connectivity status!
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        setState(() {
          isConnected = true;
        });
      } else {
        setState(() {
          isConnected = false;
        });
      }
      if (isConnected == false) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => noConnection()));
      }
    });

    print(widget.fromFood);
    print(widget.foodIndex);
    print(modelShops.id);

//     WidgetsBinding.instance.addPostFrameCallback((timeStamp) { _scaffoldKey.currentState.showBottomSheet((context) => bottom_sheet(modelShops));});
    print(categoryListId.length);
    scrollController = ScrollController();
  }

  changeState() {
    setState(() {
      isInBasket = !isInBasket;
    });
    print(isInBasket);
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        body: (modelProduct.length > 0)
            ? DefaultTabController(
                length: 3,
                child: SafeArea(
                    child: Stack(
                  children: <Widget>[
                    NestedScrollView(
                      scrollDirection: Axis.vertical,
                      physics: AlwaysScrollableScrollPhysics(),
                      controller: scrollController,
                      headerSliverBuilder:
                          (BuildContext context, bool innerBoxIsScrolled) {
                        return <Widget>[
                          SliverAppBar(
                              textTheme: TextTheme(
//                          headline6: TextStyle(fontSize: 1),
//                          headline5: TextStyle(fontSize: 1),
//                          headline4: TextStyle(fontSize: 1),
//                          headline3: TextStyle(fontSize: 1),
//                          headline2: TextStyle(fontSize: 1),
//                          headline1: TextStyle(fontSize: 1),
//                          subhead: TextStyle(fontSize: 1),
//                          headline: TextStyle(fontSize: 1),
                                  ),
                              primary: true,
                              floating: false,
                              pinned: false,
                              forceElevated: false,
                              expandedHeight: 200,
                              flexibleSpace: FlexibleSpaceBar(
                                title: GestureDetector(
                                  child: Card(
                                    child: Container(
                                      decoration:
                                          BoxDecoration(color: Colors.grey[50]),
                                      padding: EdgeInsets.all(5),
                                      width: MediaQuery.of(context).size.width,
                                      height: 120,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      .3,
                                                  child: (modelShops
                                                              .storeName !=
                                                          null)
                                                      ? Text(
                                                          modelShops.storeName,
                                                          textAlign:
                                                              TextAlign.right,
                                                          style: TextStyle(
                                                              fontSize: 10),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        )
                                                      : Container(),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.all(5),
                                                  child: Text(
                                                    modelShops.address.city
                                                            .toString() +
                                                        modelShops
                                                            .address.street1 +
                                                        modelShops
                                                            .address.street2,
                                                    textAlign: TextAlign.right,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style:
                                                        TextStyle(fontSize: 8),
                                                    maxLines: 3,
                                                  ),
                                                  width: 100,
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      .2,
                                                  child: (modelShops
                                                                  .firstName !=
                                                              null &&
                                                          modelShops.lastName !=
                                                              null)
                                                      ? Text(
                                                          modelShops.firstName +
                                                              ' ' +
                                                              modelShops
                                                                  .lastName,
                                                          textAlign:
                                                              TextAlign.right,
                                                          style: TextStyle(
                                                              decoration:
                                                                  TextDecoration
                                                                      .none,
                                                              fontSize: 6),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        )
                                                      : Container(),
                                                  alignment: Alignment.bottomRight,
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: 70,
                                            height: 80,
                                            child: ClipOval(
                                                child: Image.network(
                                                    modelShops.gravatar)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  onTap: () => print('pressed'),
                                ),
                                centerTitle: true,
                                titlePadding: EdgeInsets.all(5),
                              )),
                        ];
                      },
                      body: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Card(
                                    elevation: 8,
                                    child: Container(
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Colors.black12),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Directionality(
                                          textDirection: TextDirection.rtl,
                                          child: ListView.builder(
                                            itemBuilder:
                                                (BuildContext context, i) {
                                              return FlatButton(
                                                child: Text(
                                                  categoryListName
                                                      .toSet()
                                                      .toList()[i],
                                                  style: TextStyle(
                                                      color: (selected == i)
                                                          ? Colors.black
                                                          : Colors.black26),
                                                ),
                                                onPressed: () {
                                                  _tabController.animateTo(i);
                                                },
                                              );
                                            },
                                            itemCount: categoryListName
                                                .toSet()
                                                .toList()
                                                .length,
                                            scrollDirection: Axis.horizontal,
                                          ),
                                        )),
                                  ),
                                ),
                              )
                            ],
                            mainAxisAlignment: MainAxisAlignment.end,
                          ),
                          Expanded(
                            flex: 1,
                            child: Directionality(
                              textDirection: TextDirection.rtl,
                              child: TabBarView(
                                  controller: _tabController,
                                  children: generateWidget()),
                            ),
                          ),
                          SizedBox(
                            height: 60,
                          ),
                        ],
                      ),
                    ),
                    (basket_list.length > 0)
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ShowCart(basket_list))),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            'ویرایش سبد خرید',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ],
                                      ),
                                      height: 60,
                                      decoration: BoxDecoration(
                                          color: Colors.yellowAccent),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),
                    (basket_list.length > 0)
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ShowCart(basket_list))),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Text(
                                            'محصول',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                          Text(
                                            PersianNumbers.toPersian(
                                                getCount()),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ],
                                      ),
                                      height: 60,
                                      decoration: BoxDecoration(
                                          color: Colors.green[500]),
                                    ),
                                    flex: 1,
                                  ),
                                  Expanded(
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Text(
                                            PersianNumbers.toPersian(Formatter.f
                                                .format(int.parse(getTotal()))),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                          Text(
                                            ' : مبلغ',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ],
                                      ),
                                      height: 60,
                                      decoration: BoxDecoration(
                                          color: Colors.green[600]),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Align(
                            alignment: Alignment.bottomCenter,
                            child: GestureDetector(
                              onTap: () => print('pressed'),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'سبد خرید شما خالی است',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    height: 60,
                                    decoration:
                                        BoxDecoration(color: Colors.red),
                                  ),
                                ],
                              ),
                            ),
                          )
                  ],
                )))
            : Center(
                child: CircularProgressIndicator(),
              ));
  }

  Widget shopCards() {
    return GestureDetector(
      child: Card(
        child: Container(
          padding: EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: 120,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * .6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'رستوران همیشه بهار',
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 13),
                          ),
                          width: 150,
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'میدان مشتاق خیابان هیفده شهریور',
                            textAlign: TextAlign.right,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 13),
                          ),
                          width: 180,
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'دلیوری سنتر',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 12),
                        ),
                        Container(
                          child: Text(
                            'رستوران و فست فود',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 13),
                          ),
                          width: 150,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.3,
                height: 90,
                child: ClipOval(
                  child: Image.asset('images/feresta.jpg'),
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: () => print('pressed'),
    );
  }

  Widget itemCard(int index, TextEditingController editingController) {
    return GestureDetector(
      onTap: () => {show_bottom_sheet(index, editingController)},
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.all(8),
          margin: EdgeInsets.all(10),
          width: 150,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                      width: 200,
                      height: 110,
                      child: Image.network(
                          itembyCategory[index].proImages[0].src)),
                ],
              ),
              Text(
                itembyCategory[index].name,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.right,
              ),
              Text(
                HtmlParser.parseHtmlString(itembyCategory[index].description),
                style: TextStyle(fontSize: 10, color: Colors.grey),
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                maxLines: 4,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(' تومان  '),
                  (itembyCategory[index].price != null && itembyCategory[index].price != '')?Text(
                    PersianNumbers.toPersian(Formatter.f
                        .format(int.parse(itembyCategory[index].price))),
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        decoration: (itembyCategory[index].onSale)
                            ? TextDecoration.lineThrough
                            : TextDecoration.none),
                  ):Text(itembyCategory[index].price),
                ],
              ),
              Center(
                  child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Container(
                        height: 36,
//                      child: (editingController.text == '0')
//                          ? OutlineButton(
//                              splashColor: Colors.greenAccent,
//                              child: Text(
//                                'افزودن به سبد خرید',
//                                maxLines: 1,
//                                style: TextStyle(fontSize: 14),
//                              ),
//                              borderSide: BorderSide(
//                                  width: 1, color: Colors.greenAccent),
//                              onPressed: () {
//                                changeState();
//                                show_bottom_sheet(index, editingController);
//                              },
//                            )
//                          : OutlineButton(
//                              splashColor: Colors.yellow,
//                              child: Row(
//                                mainAxisAlignment:
//                                    MainAxisAlignment.spaceEvenly,
//                                children: <Widget>[
//                                  Text('تغییر'),
//                                  Text(
//                                    'x' +
//                                        PersianNumbers.toPersian(
//                                            editingController.text),
//                                    style: TextStyle(fontSize: 16),
//                                    maxLines: 1,
//                                  ),
//                                ],
//                              ),
//                              borderSide:
//                                  BorderSide(width: 1, color: Colors.yellow),
//                              onPressed: () {
//                                show_bottom_sheet(index, editingController);
//                              },
//                            )),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              width: 36,
                              height: 36,
                              child: RaisedButton(
                                child: Text(
                                  '-',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                color: Colors.red,
                                onPressed: () {
                                  minusToCard(index, editingController);
                                  addToProductList(
                                      bottom_items[index].id,
                                      bottom_items[index].price,
                                      int.parse(editingController.text),
                                      index,
                                      bottom_items[index].name);
                                },
                              ),
                            ),
                            Container(
                              width: 45,
                              height: 45,
                              alignment: Alignment.center,
                              child: Center(
                                child: TextField(
                                    textAlignVertical: TextAlignVertical.top,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black12,
                                                width: 1)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black12,
                                                width: 1))),
                                    controller: editingController,
                                    textAlign: TextAlign.center,
                                    textDirection: TextDirection.rtl,
                                    keyboardType: TextInputType.number,
                                    onChanged: (String str) {}),
                              ),
                            ),
                            Container(
                              width: 36,
                              height: 36,
                              child: RaisedButton(
                                child: Text(
                                  '+',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                color: Colors.green,
                                onPressed: () {
                                  addToCard(index, editingController);
                                  addToProductList(
                                      bottom_items[index].id,
                                      bottom_items[index].price,
                                      int.parse(editingController.text),
                                      index,
                                      bottom_items[index].name);
                                },
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              )),
            ],
          ),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
        ),
      ),
    );
  }

  BottomSheet show_bottom_sheet(
      int index, TextEditingController editingController) {
    _scaffoldKey.currentState
        .showBottomSheet((context) => bottom_sheet(index, editingController));
  }

  Container bottom_sheet(int index, TextEditingController editingController) {
    print(count);
    return Container(
      decoration: BoxDecoration(
          color: Colors.white60,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10))),
      padding: EdgeInsets.all(8),
      alignment: Alignment.centerRight,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .7,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .3,
            child: Image.network(bottom_items[index].proImages[0].src),
          ),
          Center(
            child: Text(
              bottom_items[index].name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                'توضیحات',
                textAlign: TextAlign.right,
                style: TextStyle(fontSize: 18),
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: 10,
              ),
              Text(HtmlParser.parseHtmlString(bottom_items[index].description),
                  textAlign: TextAlign.right),
            ],
          ),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Expanded(
//                flex: 1,
//                child: Center(
//                    child: Row(
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    Container(
//                      width: 36,
//                      height: 36,
//                      child: OutlineButton(
//                        child: Text('+'),
//                        borderSide:
//                            BorderSide(width: 1, color: Colors.greenAccent),
//                        onPressed: () {
////                          addToCard(index, editingController);
//                          addToProductList(
//                              index,
//                              bottom_items[index].price,
//                              int.parse(editingController.text),
//                              index,
//                              bottom_items[index].name);
//                        },
//                      ),
//                    ),
//                    Container(
//                      width: 20,
//                      child: TextField(
//                          controller: editingController,
//                          textAlign: TextAlign.center,
//                          keyboardType: TextInputType.number,
//                          onChanged: (String str) {}),
//                    ),
//                    Container(
//                      width: 36,
//                      height: 36,
//                      child: OutlineButton(
//                        child: Text('-'),
//                        borderSide:
//                            BorderSide(width: 1, color: Colors.greenAccent),
//                        onPressed: () {
////                          minusToCard(index, editingController);
//                          addToProductList(
//                              index,
//                              bottom_items[index].price,
//                              int.parse(editingController.text),
//                              index,
//                              bottom_items[index].name);
//                        },
//                      ),
//                    ),
//                  ],
//                )),
//              ),
//              Expanded(
//                flex: 1,
//                child: Center(
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.end,
//                    crossAxisAlignment: CrossAxisAlignment.end,
//                    children: <Widget>[
//                      Text(' تومان  '),
//                      Text(PersianNumbers.toPersian(Formatter.f.format(int.parse(bottom_items[index].price)))
//                          .toString()),
//                    ],
//                  ),
//                ),
//              )
//            ],
//          ),
          Center(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Container(
                    width: 36,
                    height: 36,
                    child: RaisedButton(
                        child: Text('بستن'),
                        color: Colors.greenAccent,
                        splashColor: Colors.yellow,
                        focusColor: Colors.yellow,
                        textColor: Colors.white,
                        hoverColor: Colors.yellow,
                        clipBehavior: Clip.antiAlias,
                        onPressed: () {
                          print(isInBasket);

                          Navigator.pop(
                              context); //print(searchInBasketList(bottom_items[index].id));
                        })),
              ),
            ],
          )),
        ],
      ),
    );
  }

  get_products() async {
    store_id = widget.store_id;
    List<ModelProducts> items = List();

    int cat_id;
    modelProduct = new List<ModelProducts>();
    var dio = new Dio();
    dio
      ..interceptors.add(RetryInterceptor(
          options: const RetryOptions(
        retries: 10,
        retryInterval: Duration(seconds: 3),
      )));
    Response response = await dio.get(
        'https://www.feresta.ir/wp-json/dokan/v1/stores/${store_id}/products?per_page=100');
    setState(() {
      int c = 0;
      for (var n in response.data) {
        modelProduct.add(ModelProducts.fromJson(n));
//        _controller.add(new TextEditingController());
//        _controller[c].text = '0';
//        c++;

      }
    });

    for (var m in modelProduct) {
      categoryListId.add(m.categories[0].id);
      categoryListName.add(m.categories[0].name);
//      if (m.categories[0].id == categoryListId.toSet().toList()[0]) {
//        items.add(m);
//        setState(() {
//          bottom_items = items;
//
//        });
//      }

    }
    finalCategoryListId = categoryListId.toSet().toList();
    print(categoryListId.toSet().toList());
    setState(() {
      _tabController = TabController(
          vsync: this, length: categoryListId.toSet().toList().length);
    });
    for (var n in modelProduct) {
      if (n.categories[0].id == categoryListId.toSet().toList()[0]) {
        items.add(n);
      }
      setState(() {
        bottom_items = items;
      });
    }
    _tabController.addListener(() {
      setState(() {
        selected = _tabController.index;
      });
      items.clear();
      for (var n in modelProduct) {
        if (n.categories[0].id ==
            categoryListId.toSet().toList()[_tabController.index]) {
          items.add(n);
        }
      }
      setState(() {
        bottom_items = items;
        print(bottom_items[0].name);
      });
    });

    categoryListName.toSet().toList();
    finalCategoryListName = categoryListName.toSet().toList();
    print(categoryListName.toSet().toList());
//        for(int i = 0 ;i<modelProduct.length;i++){
//          _controller.add(new TextEditingController());
//          _controller[i].text = '0';
//
//
//    }
//        setState(() {
//          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
//             _scaffoldKey.currentState.showBottomSheet((context) => show_bottom_sheet(widget.foodIndex));
//          });
    //  });

//        if(widget.fromFood==true && widget.foodIndex != null){
//          WidgetsBinding.instance.addPostFrameCallback((_){
//
//            _scaffoldKey.currentState.showBottomSheet((context) => show_bottom_sheet(widget.foodIndex));
//
//          });
//        }
  }

  addToCard(int index, TextEditingController editingController) {
    int count = int.parse(editingController.text);
    count += 1;
    editingController.text = count.toString();
  }

  minusToCard(int index, TextEditingController editingController) {
    int count = int.parse(editingController.text);
    if (count > 0) {
      count -= 1;
      editingController.text = count.toString();
    }
  }

  addToProductList(int productId, String price, int count, index, name) {
    if (count > 0) {
      if (list.length > 0) {
        list.removeWhere((element) => element.product_id == productId);
        list.add(new Product(
            product_id: productId, quantity: count, price: price, Name: name));
      } else {
        list.add(new Product(
            product_id: productId, quantity: count, price: price, Name: name));
      }
    } else {
      if (list.length > 0) {
        list.removeWhere((element) => element.product_id == productId);
      }
    }

//    else {;}
//
    setState(() {
      basket_list = list;
      product_count = count.toString();
    });
  }

  String getTotal() {
    int total = 0;

    for (int i = 0; i < basket_list.length; i++) {
      total += int.parse(basket_list[i].price) * basket_list[i].quantity;
    }
    return total.toString();
  }

  String getCount() {
    int count = 0;

    for (int i = 0; i < basket_list.length; i++) {
      count += basket_list[i].quantity;
    }
    return count.toString();
  }

  getProductByCategoryId(int id) {
    List<ModelProducts> items = List();
    for (var n in modelProduct) {
      if (n.categories[0].id == id) {
        items.add(n);
      }
    }
    setState(() {
      itembyCategory = items;
    });
  }

  List<Widget> generateWidget() {
    _widgets.clear();
    for (int i = 0; i < categoryListId.toSet().toList().length; i++) {
      getProductByCategoryId(categoryListId.toSet().toList()[i]);
      Widget widget = GridView.count(
//                            scrollDirection: Axis.vertical,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: .1,
          childAspectRatio: 0.6,
          children: List.generate(itembyCategory.length, (index) {
            TextEditingController editingController =
                new TextEditingController();

            return Directionality(
              child: itemCard(index, editingController..text = '0'),
              textDirection: TextDirection.ltr,
            );
//            return itemCard(index, editingController..text = '0');
          }));
      _widgets.add(widget);
    }
    return _widgets;
  }

  getCurrentIndex(int id) {
    for (int i = 0; i < modelProduct.length; i++) {
      if (modelProduct[i].id == id) {
        return i;
      } else {
        print('not found');
      }
    }
  }

//int searchInBasketList(id){
//    for(int i =0;i<basket_list.length;i++){
//      if(basket_list[i].product_id==id && basket_list[i].quantity>0){
//          return basket_list[i].quantity;
//
//      }
//
//      else{
//          return 0;
//        }
//    }
//      return 0;
//
//}
}
