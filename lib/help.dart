import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Help extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HelpState();
  }

}
class HelpState extends State<Help>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              SizedBox(height: 50,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('راهنمای ثبت سفارش',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
              ),

              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('1.به صفحه فروشگاه مورد نظر خود بروید.',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('2.محصول و غذاها و دسرها و نوشیدنی خود را انتخاب کنید.',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('3.نشانی و آدرس خود را وارد نمایید.',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('4.نوع پرداخت خود را انتخاب نمایید.',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('5.در را باز کنید سفارش شما آماده است!',textAlign: TextAlign.right,),
              )
            ],
          ),

        ),
      ),
    );
  }

}