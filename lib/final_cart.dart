import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:ferestaapp/data_models/basket_product.dart';
import 'package:ferestaapp/data_models/model_products.dart';
import 'package:ferestaapp/drawer.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:ferestaapp/payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:persian_numbers/persian_numbers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

import 'helpers/connection.dart';

class FinalCart extends StatefulWidget {
  List<Product> products;
  FinalCart(this.products);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FinalCartState();
  }
}
enum SingingCharacter { out_of_city,in_city,around_city,in_place,sardasht }
enum payment { wallet, cash }

class FinalCartState extends State<FinalCart> {
  bool onLoad = false;
  int shippingPrice = 3000;
  SingingCharacter _character = SingingCharacter.in_place;
  payment _payment = payment.wallet;
  TextEditingController addressController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController familyController = TextEditingController();
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  bool isConnected;
  String shipping_way = 'local_pickup';
  static List<Product> product_list;
  String city;
  var subscription;
  String name;
  String family;
  String address;
  String mobile;
  @override
  void initState() {
    product_list = widget.products;
    getCity();
//    Connection.checkConnection(context);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {
      // Got a new connectivity status!
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        setState(() {
          isConnected = true;
        });
      }
      else {
        setState(() {
          isConnected = false;
        });
      }
      if (isConnected == false) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => noConnection()));
      }
    });
  }

  var f = NumberFormat('##,###');

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: key,
      appBar: AppBar(

        elevation: 0,
        automaticallyImplyLeading: false,
        title: Container(
          alignment: Alignment.centerLeft,
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: Image.asset(
              'images/feresta.jpg'),
        ),
        actions: <Widget>[
          FlatButton(child: Text('حذف سبد خرید'),onPressed: ()=>Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>MainPage()), (route) => false),),
        ],
      ),
      body: (onLoad == false)?Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),
                child: Column(
                  children: <Widget>[

                    Container(
                      padding: EdgeInsets.only(top: 5),
                      height: 40,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Container(
                        width: double.maxFinite,
                        child: Text(
                          'لیست سفارش ها',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.yellowAccent
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: (product_list.length > 0) ? ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (context, postion) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Text(product_list[postion].Name),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text('تومان   '),
                                        Text(PersianNumbers.toPersian(f.format(
                                            int.parse(
                                                product_list[postion].price))))
                                      ],
                                    ),
                                    Text(PersianNumbers.toPersian(
                                        product_list[postion].quantity
                                            .toString()) + '  عدد ')
                                  ],
                                ),

                                SizedBox(height: 10,),
                                Divider()
                              ],

                            ),
                          );
                        },
                        itemCount: product_list.length,) : Center(
                        child: Text('سبد شما خالی است'),),
                    ),


                  ],
                ),
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),

                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10,),
                    Text('مشخصات فردی', textAlign: TextAlign.right,),
                    TextField(controller: nameController,
                      decoration: InputDecoration(hintText: 'نام'),
                      textAlign: TextAlign.right,
                      maxLines: 1,),
                    SizedBox(height: 10,),
                    TextField(controller: familyController,
                      decoration: InputDecoration(hintText: ' نام خانوادگی'),
                      textAlign: TextAlign.right,
                      maxLines: 1,)

                    , SizedBox(height: 10,),
                    TextField(controller: addressController,
                      decoration: InputDecoration(hintText: 'آدرس'),
                      textAlign: TextAlign.right,
                      maxLines: 3,)
                    , SizedBox(height: 10,),
                    TextField(
                      controller: mobileController, textAlign: TextAlign.right,
                      decoration: InputDecoration(hintText: 'شماره موبایل'),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(margin: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Text('نوع پرداخت', textAlign: TextAlign.center,),
                    ListTile(

                      title: const Text(
                        'پرداخت آنلاین', textAlign: TextAlign.right,style: TextStyle(fontSize: 13)),
                      leading: Radio(
                        value: payment.wallet,
                        groupValue: _payment,
                        onChanged: (payment value) {
                          setState(() {
                            _payment = value;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'پرداخت درب منزل', textAlign: TextAlign.right,style: TextStyle(fontSize: 13)),
                      leading: Radio(
                        value: payment.cash,
                        groupValue: _payment,
                        onChanged: (payment value) {
                          setState(() {
                            _payment = value;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(margin: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Text('نوع حمل و نقل', textAlign: TextAlign.center,),
                    ListTile(

                      title: const Text(
                        'خارج شهر ۲۰۰۰۰ تومان', textAlign: TextAlign.right,style: TextStyle(fontSize: 13),),
                      leading: Radio(
                        value: SingingCharacter.out_of_city,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                            shippingPrice = 20000;
                            setState(() {
                              shipping_way = 'flat_rate';
                            });
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        ' داخل شهر ۳۰۰۰ تومان', textAlign: TextAlign.right,style: TextStyle(fontSize: 13)),
                      leading: Radio(
                        value: SingingCharacter.in_city,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                            shippingPrice = 3000;
                            shipping_way = 'flat_rate';
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text('حومه شهر ۱۰۰۰۰ تومان', textAlign: TextAlign.right,style: TextStyle(fontSize: 13)),
                      leading: Radio(
                        value: SingingCharacter.around_city,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                            shippingPrice = 10000;
                            shipping_way = 'flat_rate';
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'فلکه سردشت تا آخر کهنه خانه ۴۰۰۰ تومان', textAlign: TextAlign.right,style: TextStyle(fontSize: 13)),
                      leading: Radio(
                        value: SingingCharacter.sardasht,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                            shippingPrice = 4000;
                            shipping_way = 'flat_rate';
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'تحویل در فروشگاه', textAlign: TextAlign.right,),
                      leading: Radio(
                        value: SingingCharacter.in_place,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                            shipping_way = 'local_pickup';
                          });
                        },
                      ),
                    ),


                  ],
                ),
              ),
            ),
            RaisedButton(
              child: Text('ثبت سفارش'),
              onPressed: () {
                 name = nameController.text.trim();
                 family = familyController.text.trim();
                 address = addressController.text.trim();
                 mobile = mobileController.text.trim();

                if (name != '' && family != '' && address != '' &&
                    mobile != '') {
                  if(_payment.index == 0){
                    payOnline();
                  }
                  else if(_payment.index == 1){
                    payInPlace();
                  }
                } else {
                  key.currentState.showSnackBar(new SnackBar(content: Text(
                    'مشخصات وارد شده صحیح نمی باشد',
                    textAlign: TextAlign.right,),
                    backgroundColor: Colors.redAccent,),);
                }
              }
              ,
              color: Colors.greenAccent,
            )
          ],
        ),
      ):Center(child: CircularProgressIndicator(),),
    );
  }

  getCartShared() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//    List<ModelProducts> list = sharedPreferences.getStringList('cart');
  }

  getCity() async {
    city = await user_shared.getCity();
    print(city);
  }

  payInPlace() async {
    if(_character.index ==3){
      shipping_way = 'local_pickup';
    }
    else{
      shipping_way = 'flat_rate';
    }
    setState(() {
      onLoad = true;
    });
    String token = await user_shared.getUserToken();
    var products = [];
    if(product_list.length>0){
      for (int i = 0; i < product_list.length; i++) {
        products.add([product_list[i].product_id,product_list[i].quantity]);
      }
    }

//    var url = 'https://www.feresta.ir/wp-json/ej_rest/v1/order?auth_token=${token}&datas={"first_name":"${nameController.text}","last_name":"${familyController.text}","address":"${addressController.text}","phonenum":"${mobileController.text}"}&products=${products}&shipping_way=local_pickup&payment=cash&shipping_price=${getTotal()}';
    var dio = Dio();
    var  url ="https://www.feresta.ir/wp-json/ej_rest/v1/order";
//    Response response = await dio.get(url);
//    Response response = await dio.get(url, queryParameters: {
//      "auth_token": token,
//      "datas": {"first_name":"saaed","last_name":"nabati","address":"neyshabur","phonenum":"09159529875"},
//      "products": products,
//      "payment":"cash",
//      "shipping_way": "local_pickup",
//      "shipping_price": getTotal()
//    });
    Response response = await dio.get('https://www.feresta.ir/wp-json/ej_rest/v1/order?auth_token=$token&datas={"first_name":"$name","last_name":"$family","address":"$address","phonenum":"$mobile"}&products=$products&shipping_way=$shipping_way&payment=cash&shipping_price=${getTotal()}');
    setState(() {
      onLoad = false;
    });
    print('token is : '+token);
    print(products);
    print(getTotal());
    print("${nameController.text}");
    print(familyController.text);
    print(addressController.text);
    print(mobileController.text);
    print(response.data['code']);
    if(response.data['status'] == 'success'){
      key.currentState.showSnackBar(new SnackBar(content: Text('عملیات سفارش با موفقیت انجام شد'),backgroundColor: Colors.greenAccent,));
    }
    else{      key.currentState.showSnackBar(new SnackBar(content: Text('خطا در انجام عملیات'),backgroundColor: Colors.redAccent,));
    }
  }

  payOnline() async {
    if(_character.index ==3){
      shipping_way = 'local_pickup';
    }
    else{
      shipping_way = 'flat_rate';
    }
    print(shipping_way);
    setState(() {
      onLoad = true;
    });
    print('working');
    var products = [];
    String token =await user_shared.getUserToken();
    for (int i = 0; i < product_list.length; i++) {
      products.add([product_list[i].product_id,product_list[i].quantity]);

//      products[i] = [product_list[i].product_id, product_list[i].quantity];
//    print(product_list[i].product_id);
    }
    var url = 'https://www.feresta.ir/wp-json/ej_rest/v1/orderp?auth_token=$token&datas={"first_name":"$name","last_name":"$family","address":"$address","phonenum":"$mobile"}&products=$products&shipping_way=$shipping_way&payment=cash&shipping_price=${getTotal()}';
    var dio = Dio();
    Response response = await dio.get(url);
    setState(() {
      onLoad = false;
    });
    if(response.data['status'] == 'success'){
      var pay_url = response.data['url'];
      Navigator.push(context, MaterialPageRoute(builder: (_)=>Payment(pay_url)));

    }
  }

  int getTotal() {
    int total = 0;

    for (int i = 0; i < product_list.length; i++) {
      total += int.parse(product_list[i].price) * product_list[i].quantity;
    }
    return total;
  }
}