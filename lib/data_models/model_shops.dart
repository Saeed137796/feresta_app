// To parse this JSON data, do
//
//     final modelShops = modelShopsFromJson(jsonString);

import 'dart:convert';

ModelShops modelShopsFromJson(String str) => ModelShops.fromJson(json.decode(str));

String modelShopsToJson(ModelShops data) => json.encode(data.toJson());

class ModelShops {
  int id;
  String storeName;
  String firstName;
  String lastName;
  Social social;
  String phone;
  bool showEmail;
  Address address;
  String location;
  String banner;
  int bannerId;
  String gravatar;
  int gravatarId;
  String shopUrl;
  int productsPerPage;
  bool showMoreProductTab;
  bool tocEnabled;
  String storeToc;
  bool featured;
  Rating rating;
  bool enabled;
  DateTime registered;
  String payment;
  bool trusted;
  Links links;

  ModelShops({
    this.id,
    this.storeName,
    this.firstName,
    this.lastName,
    this.social,
    this.phone,
    this.showEmail,
    this.address,
    this.location,
    this.banner,
    this.bannerId,
    this.gravatar,
    this.gravatarId,
    this.shopUrl,
    this.productsPerPage,
    this.showMoreProductTab,
    this.tocEnabled,
    this.storeToc,
    this.featured,
    this.rating,
    this.enabled,
    this.registered,
    this.payment,
    this.trusted,
    this.links,
  });

  factory ModelShops.fromJson(Map<String, dynamic> json) => ModelShops(
    id: json["id"],
    storeName: json["store_name"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    social: Social.fromJson(json["social"]),
    phone: json["phone"],
    showEmail: json["show_email"],
    address: Address.fromJson(json["address"]),
    location: json["location"],
    banner: json["banner"],
    bannerId: json["banner_id"],
    gravatar: (json['gravatar'] is String)?json["gravatar"]:'',
    gravatarId: json["gravatar_id"],
    shopUrl: json["shop_url"],
    productsPerPage: json["products_per_page"],
    showMoreProductTab: json["show_more_product_tab"],
    tocEnabled: json["toc_enabled"],
    storeToc: json["store_toc"],
    featured: json["featured"],
    rating: Rating.fromJson(json["rating"]),
    enabled: json["enabled"],
    registered: DateTime.parse(json["registered"]),
    payment: json["payment"],
    trusted: json["trusted"],
    links: Links.fromJson(json["_links"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_name": storeName,
    "first_name": firstName,
    "last_name": lastName,
    "social": social.toJson(),
    "phone": phone,
    "show_email": showEmail,
    "address": address.toJson(),
    "location": location,
    "banner": banner,
    "banner_id": bannerId,
    "gravatar": gravatar,
    "gravatar_id": gravatarId,
    "shop_url": shopUrl,
    "products_per_page": productsPerPage,
    "show_more_product_tab": showMoreProductTab,
    "toc_enabled": tocEnabled,
    "store_toc": storeToc,
    "featured": featured,
    "rating": rating.toJson(),
    "enabled": enabled,
    "registered": registered.toIso8601String(),
    "payment": payment,
    "trusted": trusted,
    "_links": links.toJson(),
  };
}

class Address {
  String street1;
  String street2;
  String city;
  String zip;
  String country;
  String state;

  Address({
    this.street1,
    this.street2,
    this.city,
    this.zip,
    this.country,
    this.state,
  });

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    street1: json["street_1"],
    street2: json["street_2"],
    city: json["city"],
    zip: json["zip"],
    country: json["country"],
    state: json["state"],
  );

  Map<String, dynamic> toJson() => {
    "street_1": street1,
    "street_2": street2,
    "city": city,
    "zip": zip,
    "country": country,
    "state": state,
  };
}

class Links {
  List<Collection> self;
  List<Collection> collection;

  Links({
    this.self,
    this.collection,
  });

  factory Links.fromJson(Map<String, dynamic> json) => Links(
    self: List<Collection>.from(json["self"].map((x) => Collection.fromJson(x))),
    collection: List<Collection>.from(json["collection"].map((x) => Collection.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "self": List<dynamic>.from(self.map((x) => x.toJson())),
    "collection": List<dynamic>.from(collection.map((x) => x.toJson())),
  };
}

class Collection {
  String href;

  Collection({
    this.href,
  });

  factory Collection.fromJson(Map<String, dynamic> json) => Collection(
    href: json["href"],
  );

  Map<String, dynamic> toJson() => {
    "href": href,
  };
}

class Rating {
  String rating;
  int count;

  Rating({
    this.rating,
    this.count,
  });

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
    rating: json["rating"],
    count: json["count"],
  );

  Map<String, dynamic> toJson() => {
    "rating": rating,
    "count": count,
  };
}

class Social {
  String fb;
  String gplus;
  String youtube;
  String twitter;
  String linkedin;
  String pinterest;
  String instagram;
  String flickr;

  Social({
    this.fb,
    this.gplus,
    this.youtube,
    this.twitter,
    this.linkedin,
    this.pinterest,
    this.instagram,
    this.flickr,
  });

  factory Social.fromJson(Map<String, dynamic> json) => Social(
    fb: json["fb"],
    gplus: json["gplus"],
    youtube: json["youtube"],
    twitter: json["twitter"],
    linkedin: json["linkedin"],
    pinterest: json["pinterest"],
    instagram: json["instagram"],
    flickr: json["flickr"],
  );

  Map<String, dynamic> toJson() => {
    "fb": fb,
    "gplus": gplus,
    "youtube": youtube,
    "twitter": twitter,
    "linkedin": linkedin,
    "pinterest": pinterest,
    "instagram": instagram,
    "flickr": flickr,
  };
}
