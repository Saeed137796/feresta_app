import 'package:ferestaapp/city_select.dart';
import 'package:ferestaapp/food.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:ferestaapp/shops.dart';
import 'package:ferestaapp/showCart.dart';
import 'package:ferestaapp/splash_screen.dart';
import 'package:flutter/material.dart';
import 'final_cart.dart';
import 'login.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      showSemanticsDebugger: false,
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'iran',
        primaryColor: Colors.white,
        primaryColorDark: Colors.white,



        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen()
    );
  }
}


