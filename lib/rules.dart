import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Rules extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RulesState();
  }

}
class RulesState extends State<Rules>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 50,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('قوانین و مقررات',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                ),

                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('فرستا (feresta.ir) تابع قوانین حاکم کشور می باشد و هر گونه فعالیت مخالف قوانین سایت و کشور توسط کاربر باعث حذف اکانت کاربر مورد نظر خواهد شد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('فرستا صرفا به عنوان نماینده رستوران و فروشگاههای خدمات خود را ارائه می نماید.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('فرستا نماینده‌ای برای برقراری ارتباط بین مشتریان آنلاین و رستوران‌های  فروش غذا است و طبخ غذا با رستوران می‌باشد. بنابراین مسئولیت کیفیت غذا و نحوه‌ی ارسال سفارش بر عهده رستوران می‌باشد. فرستا اطمینان می‌دهد بستری برای انتقال هرگونه شکایت، درخواست و اظهار نظر در مورد کیفیت و نحوه‌ی ارسال غذا به مدیریت رستوران ایجاد نماید.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('در صورت نیاز با شماره ی شما تماس گرفته می‌شود. در صورت عدم دریافت پاسخ از شما سفارش لغو می‌گردد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('بعد از ثبت یک سفارش از جانب شما دیگر امکان لغو آن وجود نخواهد داشت. شما می‌توانید در صورت ضرورت هرگونه تغییر در سفارش با پشتیبانی فرستا از طریق چت یا تماس ارتباط برقرار نمایید.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('تلاش فرستا و مجموعه‌های همکار بر ارسال سریع و بدون نقص سفارش شماست، با وجود این مدت زمان ذکر شده برای تحویل سفارش بازه‌ای تقریبی بوده و این امکان وجود دارد تا تحت اثر شرایط خارج از کنترل رستوران یا و فروشگاهها این زمان افزایش یابد و یا حتی منجر به لغو سفارش گردد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('ر صورتی که هزینه غذا و یا ارسال افزایش پیدا کند، پیش از تایید سفارش به شما اطلاع داده خواهد شد و هنگام تحویل سفارش هیچ وجهی به صورت نقدی از شما دریافت نخواهد شد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('مسئولیت پرداخت آنلاین به عهده بانک و درگاه مربوطه می‌باشد. در صورت بروز مشکل پرداخت از جانب بانک، مبلغ پرداختی از طریق درگاه مورد استفاده شما به حساب مبدا بازپرداخت خواهد شد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('مسئولیت صحت ورود اطلاعاتی از قبیل نام و نام خانوادگی، آدرس و شماره تماس به عهده کاربر است. در صورت ثبت اطلاعات اشتباه و به موجب آن هر گونه وقفه و یا عدم تحویل سفارش، هزینه سفارش و ارسال غیر قابل بازگشت خواهد بود.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('فرستا این حق را برای خود محفوظ می دارد تا به صورت مستقل، در هر زمان و بنابر صلاح دید خود هر سفارش و یا نام کاربری را لغو نماید.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('مسئولیت محتوا و منوی غذای رستورانها و فروشگاهها و همچنین درستی و صحت اطلاعات نمایش داده شده و تطابق آن با آنچه که عرضه می شود بر عهده مدیر رستوران و فروشگاه می باشد و فرستا هیچگونه مسئولیتی در قبال سوء استفاده‌ و آسیب های احتمالی آن بر عهده نمی‌گیرد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('فروش فرستا براساس انبار و ذخیره رستوران و فروشگاه است و اگر در موقع سفارش، محصول مورد نظر در رستوران موجود نباشد ممکن است آن سرویس ارائه نشود و فرستا نسبت به اینکه لیست غذاها و محصول ذکر شده در سایت حتماً موجود باشد تعهدی نمی دهد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('در سایت امکان اضافه کردن چند آدرس در پروفایل شما قرار داده شده است و برای این کار نیازی به ایجاد چند نام کاربری نمی باشد. لذا کاربر تنها امکان انتخاب یک نام کاربری را داشته و در صورت فعالیت یک کاربر با دو اکانت، اکانت دوم حذف خواهد شد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('گر کاربر در حین سفارش در آدرسی که مشخص کرده است نباشد سفارش به هیچ عنوان به جای دیگری که حضور دارد تحویل داده نمی شود و مسئولیت تمامی مشکلاتی که از این نظر پیش خواهد آمد بر عهده کاربر خواهد بود.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('تمامی قوانین تا زمانی که در سایت فرستا (feresta.ir) نمایش داده می‌شوند معتبر هستند. در هر بازدید شما، این امکان وجود دارد که این قوانین اصلاح و یا به روز شده باشند لذا توصیه می‌شود برای هر بار استفاده از وب سایت مجددا قوانین سایت را مطالعه نمایید.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('مطالعه این قوانین از سوی شما مسئولیت قبول شرایط در صورت استفاده از سایت را متوجه شما خواهد کرد.',textAlign: TextAlign.right,),
                ),
                SizedBox(height: 10,),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Text('امیدوار هستیم با به کارگیری این قوانین و دستورالعمل‌هایی که در آینده اضافه خواهد شد بتوانیم خدماتی مناسب به شما مشتریان عزیز ارائه کنیم. از شما تقاضا داریم با انتقادها و پیشنهادهای سازنده خود ما را در ارائه سرویس بهتر حمایت نمایید.',textAlign: TextAlign.right,),
                )
              ],
            ),
          ),

        ),
      ),
    );
  }

}