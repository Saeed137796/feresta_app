import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/data_models/basket_product.dart';
import 'package:ferestaapp/final_cart.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

import 'helpers/connection.dart';

class ConfirmNumber extends StatefulWidget{
  var number;
  List<Product> products;
  ConfirmNumber(this.number,this.products);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ConfirmNumberState();
  }

}
class ConfirmNumberState extends State<ConfirmNumber>{
  bool onLoad = false;
  bool isConnected;
  var subscription;
  TextEditingController phonenumberController = TextEditingController();
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  var phoneNumber;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldState,
      body: (onLoad == false)?(
          Container(
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Center(
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width / 3,
                    height: MediaQuery
                        .of(context)
                        .size
                        .width / 3,
                    child: Image.asset(
                        'images/feresta.jpg'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text('تایید شماره موبایل', textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 13),),
                )
                ,
                SizedBox(height: 20,)
                ,
                Center(
                  child: Text('کد تایید ارسال شده را وارد نمایید',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),),
                )
                ,
                Center(
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: phonenumberController,
                        maxLength: 4,
                        decoration: InputDecoration(
                          hintText: 'کد تایید',
                          prefix: Icon(Icons.confirmation_number)

                        ),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                      )
                  ),
                ),
                Center(
                    child: RaisedButton(
                      child: Text('تایید', textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),),
                      color: Colors.green,
                      onPressed: () =>
                         postData()
                    )
                )
              ],
            ),
          )
      ):Center(child: CircularProgressIndicator(),),
    );


  }
  postData() async{
    setState(() {
      onLoad = true;
    });
    var code_number = phonenumberController.text;
    var path = 'https://www.feresta.ir/wp-json/digits/v1/one_click?countrycode=+98&mobileNo=${phoneNumber}&otp=${code_number}';
    print(code_number);
    print(phoneNumber);
    if(code_number.length==4){
      var dio = new Dio();
      dio..interceptors.add(RetryInterceptor(
          options: const RetryOptions(
            retries: 10,
            retryInterval: Duration(seconds: 3),
          )
      ));
      Response response = await dio.post(path);
      setState(() {
        onLoad = false;
      });
      print(response.data.toString());
      if(response.statusCode==200){
        if(response.data['success']==true){
          user_shared.setUser(response.data['data']['access_token'], int.parse(response.data['data']['user_id']));
          String token = await user_shared.getUserToken();
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>FinalCart(widget.products)));

        }
        else{
          scaffoldState.currentState.showSnackBar(SnackBar(content: Text(response.data['data']['msg'],textDirection: TextDirection.rtl,textAlign: TextAlign.right),backgroundColor: Colors.redAccent,));
        }
      }
      else{
        scaffoldState.currentState.showSnackBar(SnackBar(content: Text('خطا در برقراری ارتباط',textDirection: TextDirection.rtl,textAlign: TextAlign.right,),backgroundColor: Colors.redAccent,));
      }
    }
    else{
    }


  }

  @override
  void initState() {
    super.initState();
    phoneNumber = widget.number;
//    Connection.checkConnection(context);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




    });

  }

}