import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
//import 'package:webview_flutter/webview_flutter.dart';

import 'final_cart.dart';

class Payment extends StatefulWidget{
  var url;
  Payment(this.url);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PaymentState();
  }
}
class PaymentState extends State<Payment>{
  bool onLoad = false;
  var url;

  Completer<WebViewController> _controller = Completer<WebViewController>();
  final _key = UniqueKey();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child:Stack(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: WebView(

                  key: _key,
                  initialUrl: url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
                    _controller.complete(webViewController);
                  },
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: ()=>Navigator.pop(context),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.redAccent
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width,
                    child: Center(child: Text('لغو',textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    url  = widget.url;
  }
}