import 'package:flutter/material.dart';

class CartBloc with ChangeNotifier {
  Map<int, int> _cart = {};

  Map<int, int> get cart => _cart;

  void addToCart(id_product) {
    if (_cart.containsKey(id_product)) {
      _cart[id_product] += 1;
    } else {
      _cart[id_product] = 1;
    }
    notifyListeners();
  }
  void addToCartByNumber(id_product,number) {
    if (_cart.containsKey(id_product)) {
      _cart[id_product] += number;
    } else {
      _cart[id_product] = 1;
    }
    notifyListeners();
  }

  void clear(id_product) {
    if (_cart.containsKey(id_product)) {
      _cart.remove(id_product);
      notifyListeners();
    }
  }
}