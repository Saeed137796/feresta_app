import 'dart:math';

import 'package:app_settings/app_settings.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/data_models/model_products.dart';
import 'package:ferestaapp/helpers/formatter.dart';
import 'package:ferestaapp/helpers/html_parser.dart';
import 'package:ferestaapp/shops.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:persian_numbers/persian_numbers.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:sweetalert/sweetalert.dart';
import 'drawer.dart';
import 'data_models/model_shops.dart';
import 'food.dart';
import 'helpers/connection.dart';
import 'noConnection.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  bool onLoad = true;
  int random;
  String restaurantName;
  List<ModelShops> modelShop;
  List<ModelShops> finalShops;
  List<ModelProducts> product_list;
  List<ModelShops> random_list = List();
  int id;
  var subscription;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: key,
      appBar: drawer_widget.get_appbar(context),
      body: (onLoad == false) ? Container(
        decoration: BoxDecoration(color: Colors.black12),
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: MediaQuery
            .of(context)
            .size
            .height,
        child: ListView(
          shrinkWrap: false,
          scrollDirection: Axis.vertical,
          children: <Widget>[
            shopsWidget(context),
//            SizedBox(height: 10,),
            fastfoodWidget(context),
            SizedBox(height: 20,),
            bestRestaurantWidget(context),
            whyUsWidget(context),


          ],
        ),
      ) : Center(child: CircularProgressIndicator(),),
      endDrawer: drawer_widget.getDrawer(context,key),
    );
  }


  Widget shopsWidget(context) {
    return GestureDetector(
      onTap: () =>
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => Shops()),),
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              height: MediaQuery
                  .of(context)
                  .size
                  .height * .6,
              child:new Image.asset('images/backimage.jpg',fit: BoxFit.cover,width: MediaQuery.of(context).size.width,)
            ),
            Column(
              children: <Widget>[
                Center(
//                  child: Container(
//                    decoration: BoxDecoration(
//                      borderRadius: BorderRadius.circular(50)
//                    ),
//                    margin: EdgeInsets.only(top: 20),
//                    width: MediaQuery
//                        .of(context)
//                        .size
//                        .width / 3,
//                    height: MediaQuery
//                        .of(context)
//                        .size
//                        .width / 3,
//                    child: Card(
//                      elevation: 8,
//                      child: ClipOval(
//                        child: Container(
////                          margin: EdgeInsets.only(top: 25),
//                          child: Image.asset(
//                              'images/feresta.jpg',fit: BoxFit.cover,),
//                        ),
//                      ),
//                    ),
//                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                Container(
                  alignment: Alignment.center,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: 300,
                  child: Stack(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * .8,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black,width: 1),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Image.asset('images/restaurant.png',fit: BoxFit.cover,),
                            Text(
                              'رستوران ها',
                              textAlign: TextAlign.center,
                            ),

                          ],
                        ),
                        height: 60,
                      ),
                    ),
                    Positioned(
                      top: 0,
                      left: 8,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(PersianNumbers.toPersian(
                              modelShop.length.toString()),
                            style: TextStyle(color: Colors.white),),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    )
                  ]),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  dinnerTimeWidget(context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height * .2,
            child: Image.network(
              'https://feresta.ir/wp-content/uploads/2020/03/market.png',
              fit: BoxFit.cover,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  ' منو',
                  style: TextStyle(color: Colors.white, fontSize: 23),
                  textAlign: TextAlign.right,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                alignment: Alignment.centerRight,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: 270,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    reverse: true,
                    itemCount: 10,
                    itemBuilder: (context, i) {
                      return itemCard(i);
                    }),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget fastfoodWidget(context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  ' منو $restaurantName',
                  style: TextStyle(color: Colors.black, fontSize: 23),
                  textAlign: TextAlign.right,
                ),
              ),

              SizedBox(
                height: 15,
              ),
              Container(
                alignment: Alignment.centerRight,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: 300,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    reverse: true,
                    itemCount: product_list.length,

                    itemBuilder: (context, i) {
                      return itemCard(i);
                    }),
              ),

            ],
          )
        ],
      ),
    );
  }

  Widget bestRestaurantWidget(context) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: 236,
      child: Column(
        children: <Widget>[
          Center(
            child: Text(
              'برخی از رستوران ها',
              style: TextStyle(color: Colors.black, fontSize: 23),
              textAlign: TextAlign.right,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 180,
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: ListView.builder(reverse: true,
                scrollDirection: Axis.horizontal,
                itemCount: modelShop.length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                      onTap: () =>
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) =>
                                  Food(modelShops: modelShop[i],
                                    fromFood: false,
                                    foodIndex: i,
                                    store_id: modelShop[i].id,))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 100,
                              width: 100,
                              child: ClipOval(

                                child: Image.network(modelShop[i].gravatar)
                              ),
                            ),
                            Text(modelShop[i].storeName,
                                textAlign: TextAlign.center, style: TextStyle(
                                    fontSize: 13))

                          ],
                        ),
                      )
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget whyUsWidget(context) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height - 20,
      decoration: BoxDecoration(color: Colors.black),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(height: 2,),
          Center(
            child: Text(
              'چرا فرستا؟',
              style: TextStyle(color: Colors.white, fontSize: 23),
              textAlign: TextAlign.right,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 3,
                    height: 100,
                    child: Image.asset('images/first.png')),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    child: Text('غذا و رستوران را جستجو کنید',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 15, color: Colors.white),)),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    height: 60,
                    child: Text(
                      'از بین رستوران ها و فست فود های مختلف غذای خود را با سلیقه و ذاوقه ی خود انتخاب کنید',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 12, color: Colors.white70),))
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 3,
                    height: 100,
                    child: Image.asset('images/second.png')),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    child: Text(
                      'آنلاین پرداخت کنید', textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 15, color: Colors.white),)),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    height: 60,
                    child: Text(
                      'بصورت آنلاین،امن و سریع پرداخت خود را از میان درگاه های مختلف بانکی انجام دهید',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 12, color: Colors.white70),))
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 3,
                    height: 100,
                    child: Image.asset('images/third.png')),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    child: Text('پیک اختصاصی', textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 15, color: Colors.white),)),
                Container(width: MediaQuery
                    .of(context)
                    .size
                    .width / 2,
                    height: 60,
                    child: Text(
                      'سفارش خود را به صورت گرم و بهداشتی توسط پیک اختصاصی فود سنتر تحویل بگیرید',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 12, color: Colors.white70),))
              ],
            ),
          ),

        ],
      ),
    );
  }

  Widget itemCard(index) {
    return GestureDetector(
      onTap: () =>
          Navigator.push(context, MaterialPageRoute(
              builder: (context) =>
                  Food(modelShops: finalShops[random],
                    fromFood: true,
                    foodIndex: index,
                    store_id: id,)))
      , child: Card(
      elevation: 5,
        child: Container(

        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(10),
        width: 150,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              children: <Widget>[

                Container(
                  width: 200,
                  height: 115,
                  child: Image.network( product_list[index].proImages[0].src)
//                (product_list[index].onSale)?Positioned(
//
//                  child: Container(
//                    child: Padding(
//                      padding: EdgeInsets.all(8),
//                      child: Text(product_list[index].salePrice,style: TextStyle(color: Colors.white),),
//
//                    ),
//                    decoration: BoxDecoration(color: Colors.green,borderRadius: BorderRadius.circular(5)),
//                  ),
//                  right: 0,
//                  top: 0,
//                ):Container()
                )
          ],
            ),
            SizedBox(height: 15,),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(product_list[index].name, overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.right,
                  maxLines: 2,),
                Text(HtmlParser.parseHtmlString(product_list[index].description),
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 10, color: Colors.grey),
                  textAlign: TextAlign.right,
                  maxLines: 3,),
                Text(PersianNumbers.toPersian(
                    Formatter.f.format(int.parse(product_list[index].price))) +
                    'تومان'.toString(), overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      decoration: (product_list[index].onSale) ? TextDecoration
                          .lineThrough : TextDecoration.none),),

              ],
            ),

          ],
        ),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5)),
    ),
      ),
    );
  }

  get_shops() async {
    modelShop = List<ModelShops>();
    var dio = new Dio();
    dio..interceptors.add(RetryInterceptor(
        options: const RetryOptions(
          retries: 10,
          retryInterval: Duration(seconds: 3),
        )
    ));
    Response response = await dio.get(
        'https://www.feresta.ir/wp-json/dokan/v1/stores');
    for (var n in response.data) {
      if(ModelShops.fromJson(n).storeName != 'مدیر'){
        modelShop.add(ModelShops.fromJson(n));
      }

    }
    if (this.mounted) {
      setState(() {
        finalShops = modelShop;
      });
    }

    print(finalShops.toString());
    product_list = List<ModelProducts>();

    Random rand = new Random();
    random = rand.nextInt(finalShops.length);

    id = finalShops[random].id;
    setState(() {
      restaurantName = finalShops[random].storeName;
    });
    Response response1 = await dio.get(
        'https://www.feresta.ir/wp-json/dokan/v1/stores/${id}/products?per_page=100');

    for (var n in response1.data) {
      setState(() {

        product_list.add(ModelProducts.fromJson(n));
      });
    }

    setState(() {
      onLoad = false;
    });
  }
bool isConnected;
  @override
  void initState() {
    super.initState();
    get_shops();
    getFoods(modelShop);
    getRandomShops();

//    Connection.checkConnection(context);
    print(random_list);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




    });

  }

  getFoods(List<ModelShops> list) async {
    print(product_list.toString());
  }

  getRandomShops() {
    Random random = Random();
    int rand = 0;
    int preRand = 0;
    for (int i = 0; i < modelShop.length; i++) {
      rand = random.nextInt(modelShop.length);
      if (rand != preRand) {
        random_list.add(modelShop[rand]);
      }
      else {
        rand = rand + 1;
        random_list.add(modelShop[rand]);
      }
      preRand = rand;
    }
  }
}
