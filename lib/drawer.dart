import 'package:ferestaapp/aboutUs.dart';
import 'package:ferestaapp/city_select.dart';
import 'package:ferestaapp/help.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:ferestaapp/rules.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_numbers/persian_numbers.dart';
import 'package:share/share.dart';

class drawer_widget{
  static Drawer getDrawer(BuildContext context,GlobalKey<ScaffoldState> key){
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Container(
             child: Image.asset('images/backimage.png'),
            ),
            decoration: BoxDecoration(
                color:Colors.grey[200]
            ),
          ),
          ListTile(
            onTap: (){user_shared.clearUserData();Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context)=>SelectCity()),(Route<dynamic> route) => route is SelectCity);},
            trailing: Icon(Icons.exit_to_app),
            title: Text('خروج از حساب کاربری',textAlign: TextAlign.right,),

          ),
          ListTile(
            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Rules())),
            trailing: Icon(Icons.card_giftcard),
            title: Text('قوانین و مقررات',textAlign: TextAlign.right,),

          ),
          ListTile(
            onTap: (){key.currentState.showSnackBar(new SnackBar(content: Text('به اشتراک گذاری اپلیکیشن'),action: SnackBarAction(label: 'اشتراک گذاری',onPressed: ()=>Share.share('feresta.ir', subject: 'سفارش آنلاین غذا')),),);Navigator.pop(context);}
            ,trailing: Icon(Icons.share),
            title: Text('اشتراک گذاری اپلیکیشن',textAlign: TextAlign.right,),
            
          ),
          ListTile(
            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>AboutUs())),
            trailing: Icon(Icons.call),
            title: Text('تماس با ما',textAlign: TextAlign.right,),

          ),
          ListTile(
            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Help())),
            trailing: Icon(Icons.history),
            title: Text('راهنمای ثبت سفارش',textAlign: TextAlign.right,),

          )
        ],
      ),
    );
  }
  static AppBar get_appbar(BuildContext context){
    return AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Container(
          alignment: Alignment.centerLeft,
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: Image.asset(
              'images/feresta.jpg'),
        ));
  }
}