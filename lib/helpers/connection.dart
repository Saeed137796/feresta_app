import 'package:app_settings/app_settings.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:connectivity/connectivity.dart';
import 'package:sweetalert/sweetalert.dart';

class Connection {
  static checkConnection(context) async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
    }
    else{
      return SweetAlert.show(context,title: 'عدم اتصال به اینترنت',style: SweetAlertStyle.error,showCancelButton: true,cancelButtonText: 'لغو',confirmButtonText: 'تنظیمات',onPress:(bool isConfirm){if(isConfirm){AppSettings.openDataRoamingSettings();return true;}else{return true;}return false ;});
    }
  }
}