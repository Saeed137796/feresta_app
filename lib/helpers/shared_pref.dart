import 'package:shared_preferences/shared_preferences.dart';

class user_shared{
  static String token;
  static String city;
  user_shared.setMockInitialValues();
  static setUser(String token,int user_id) async{
      SharedPreferences prefs = await SharedPreferences.getInstance();

      await prefs.setString('token', token);
      await prefs.setInt('id', user_id);
      prefs.commit();

  }
  static getUserToken() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token = prefs.getString('token');
    return token;
  }
  static clearUserData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
  static setCity(String city) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('city', city);
    prefs.commit();

  }
  static getCity() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    city = prefs.getString('city');
    return city;
  }

}