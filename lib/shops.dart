import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/data_models/model_shops.dart';
import 'package:ferestaapp/food.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:sweetalert/sweetalert.dart';
import 'drawer.dart';
import 'helpers/connection.dart';
class Shops extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ShopsState();
  }

}
class ShopsState extends State<Shops>{
  bool onLoad=true;
  List<ModelShops> modelShop;
  List<ModelShops> finalShops;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();


  var subscription;
  bool isConnected;
  @override
  void initState() {
    super.initState();
    get_shops();
//    Connection.checkConnection(context);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




    });


  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: key,
      appBar: drawer_widget.get_appbar(context),
      body: (onLoad == false)?Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(6),
        child: ListView.builder(itemCount:finalShops.length,scrollDirection: Axis.vertical,shrinkWrap: true,itemBuilder: (context,index){
          return shopCards(index);
        }),
      ):Center(child: CircularProgressIndicator(),),
      endDrawer: drawer_widget.getDrawer(context,key),

    );
  }
  Widget shopCards(int index){

    return GestureDetector(
      child: Card(
        child:Container(
          padding: EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: 110,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width*.6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(padding: EdgeInsets.all(5),child: Text(finalShops[index].storeName,textAlign: TextAlign.right,style: TextStyle(fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,),width: 150,)
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                        Container(padding: EdgeInsets.all(5),child: Text(finalShops[index].address.city.toString()+finalShops[index].address.street1+finalShops[index].address.street2,textAlign: TextAlign.right,overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 13),),width: 180,)
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(padding: EdgeInsets.all(5),child: Text(finalShops[index].firstName+' '+finalShops[index].lastName,textAlign: TextAlign.right,style: TextStyle(decoration: TextDecoration.none),),width: 150,),

                      ],
                    )
                  ],
                ),
              ),

              Container(
                width: MediaQuery.of(context).size.width*0.3,
                height: 90,
                child: Image.network(modelShop[index].gravatar)
              ),


            ],
          ),
        ),
      ),
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Food(modelShops: modelShop[index],fromFood: false,foodIndex: index,store_id: modelShop[index].id,))),
    );
  }


  get_shops() async {

    modelShop = List<ModelShops>();
    var dio = new Dio();
    dio
      ..interceptors.add(RetryInterceptor(
          options: const RetryOptions(
            retries: 10,
            retryInterval: Duration(seconds: 3),
          )
      ));
    Response response = await dio.get(
        'https://www.feresta.ir/wp-json/dokan/v1/stores');
   for (var n in response.data) {
     if(ModelShops.fromJson(n).storeName != 'مدیر'){
       modelShop.add(ModelShops.fromJson(n));
     }

    }
      setState(() {
        finalShops = modelShop;
        onLoad = false;

      });

  }
}