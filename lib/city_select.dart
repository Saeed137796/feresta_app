import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'package:flutter_typeahead/cupertino_flutter_typeahead.dart';
import 'package:sweetalert/sweetalert.dart';

import 'city_data.dart';
import 'helpers/connection.dart';
class SelectCity extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SelectCityState();
  }


}
class SelectCityState extends State<SelectCity>{
  String city='';
  bool onLoad = false;
bool isConnected;
  TextEditingController controller = TextEditingController();
var subscription;
@override
void initState() {
//  Connection.checkConnection(context);
super.initState();

  subscription = Connectivity().onConnectivityChanged.listen((
      ConnectivityResult result) {


    // Got a new connectivity status!
    if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
      isConnected = true;
    });}
    else{
      setState(() {
        isConnected = false;
      });

    }
    if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




  });

}

@override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body:(
          (onLoad ==false)?Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width/3,
                height: MediaQuery.of(context).size.width/3,
                child: Image.asset('images/feresta.jpg'),
              ),
            ),
        SizedBox(
          height: 20,
        ),
        Center(
          child: Directionality(textDirection: TextDirection.rtl,child: Text('لطفا شهر خود را انتخاب نمایید')),
        )
        ,
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TypeAheadField(
                  textFieldConfiguration: TextFieldConfiguration(
                    controller: controller,
                    textDirection: TextDirection.rtl,
                      autofocus: true,
                      textAlign: TextAlign.right,
                      style: DefaultTextStyle.of(context).style.copyWith(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.greenAccent,width: 1)
                          ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1,color: Colors.greenAccent)
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1,color: Colors.greenAccent)
                        )
                      )
                  ),
                  suggestionsCallback: (pattern) async {
                    return await BackendService.get_suggeestion(pattern);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text(suggestion['name'],textAlign: TextAlign.right,),
                    );
                  },
                  onSuggestionSelected: (suggestion) {
                    controller.text = suggestion['name'];
                    city = suggestion['name'];
                  },
                ),
              ),
            ),
            Center(
              child:RaisedButton(
                child: Text('تایید',textAlign: TextAlign.center,style: TextStyle(color:Colors.white),),
                color: Colors.green,
                onPressed: (){setState(() {
                  onLoad = true;
                });if(city != null){user_shared.setCity(city);Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MainPage()));}}
              )
            )
          ],
        ),
      ):
              Center(child: CircularProgressIndicator(),)
      ),
    );
  }

}