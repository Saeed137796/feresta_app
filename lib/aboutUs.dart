import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AboutUsState();
  }

}
class AboutUsState extends State<AboutUs>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              SizedBox(height: 50,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('نکات مهم',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
              ),

              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('این سایت فقط در محدوده پیرانشهر خدمات رسانی میکند.در هر خرید فقط از یک رستوران سفارش دهید.در صورت داشتن شکایت از مجموعه، از قسمت ثبت شکایت اقدام نمایید.',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('تماس با ما:',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('تلگرام :ferestair@',textAlign: TextAlign.right,),
              ),
              SizedBox(height: 10,),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Text('تلفن تماس: 02191011625',textAlign: TextAlign.right,),
              )
            ],
          ),

        ),
      ),
    );
  }

}