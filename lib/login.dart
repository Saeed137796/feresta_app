import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:ferestaapp/confirm_number.dart';
import 'package:ferestaapp/data_models/basket_product.dart';
import 'package:ferestaapp/noConnection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';

import 'helpers/connection.dart';

class Login extends StatefulWidget {
  List<Product> products;
  Login(this.products);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }

}

class LoginState extends State<Login> {
  bool onLoad = false;
  TextEditingController phoneNumberController = TextEditingController();
  GlobalKey<ScaffoldState> scaffoldstate = GlobalKey<ScaffoldState>();
  var subscription;
  bool isConnected;
  @override
  void initState() {
    super.initState();

//    Connection.checkConnection(context);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldstate,
      body:(onLoad==false)? (
          Container(
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Center(
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width / 3,
                    height: MediaQuery
                        .of(context)
                        .size
                        .width / 3,
                    child: Image.asset(
                        'images/feresta.jpg'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text('ثبت نام / عضویت', textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),),
                )
                ,
                SizedBox(height: 20,)
                ,
                Center(
                  child: Text('کد تایید به شماره موبایل شما ارسال می شود',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 13),),
                )
                ,
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'شماره موبایل',
                        prefix: Icon(Icons.phone_android)


                      ),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.center,
                      controller: phoneNumberController,
                      maxLength: 11,

                    )
                  ),
                ),
                Center(
                    child: RaisedButton(
                      child: Text('تایید', textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),),
                      color: Colors.green,
                      onPressed: () =>
                          postData(),
                    )
                )
              ],
            ),
          )
      ):Center(child: CircularProgressIndicator(  ),),
    );
  }
  postData() async {
    setState(() {
      onLoad = true;
    });
    var number = phoneNumberController.text;
    var path = 'https://www.feresta.ir/wp-json/digits/v1/send_otp?countrycode=+98&mobileNo=${number}';

    if (number.length == 11 || number.length == 10) {
      var dio = new Dio();
      dio..interceptors.add(RetryInterceptor(
          options: const RetryOptions(
            retries: 10,
            retryInterval: Duration(seconds: 3),
          )
      ));
      Response response = await dio.post(path);
      setState(() {
        onLoad = false;
      });
      if (response.statusCode == 200) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ConfirmNumber(number,widget.products)));
      }
    }
    else {
      scaffoldstate.currentState.showSnackBar(new SnackBar(content: Text(
        'شماره کوبایل وارد شده صحیح نمی باشد', textDirection: TextDirection.rtl,
        textAlign: TextAlign.right,), backgroundColor: Colors.redAccent));
    }
  }
}