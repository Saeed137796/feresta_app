import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ferestaapp/data_models/basket_product.dart';
import 'package:ferestaapp/final_cart.dart';
import 'package:ferestaapp/helpers/shared_pref.dart';
import 'package:ferestaapp/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_numbers/persian_numbers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ferestaapp/helpers/formatter.dart';
import 'package:sweetalert/sweetalert.dart';
import 'helpers/connection.dart';
import 'login.dart';
import 'noConnection.dart';

class ShowCart extends StatefulWidget{
  List<Product> list;
  ShowCart(this.list);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ShowCartState();
  }

}
class ShowCartState extends State<ShowCart>{
  List<Product> product_list;
  var subscription;
  String token;
  bool isConnected;

  bool isUser = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            height: 50,
            child: Image.asset(
                'images/feresta.jpg'),
          ),
        actions: <Widget>[
          FlatButton(child: Text('حذف سبد خرید'),onPressed: ()=>Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>MainPage()), (route) => false),),
        ],
      ),
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: Colors.white),
                  child: ListView.builder(itemBuilder: (BuildContext context,index){
                    return Card(
                      child: ListTile(

                        title: Text(product_list[index].Name.toString(),textDirection: TextDirection.rtl,textAlign: TextAlign.left,),
                        subtitle: Text(PersianNumbers.toPersian(product_list[index].quantity.toString())+' عدد ',textDirection: TextDirection.rtl,textAlign: TextAlign.left),
                        trailing: Text(' قیمت کل : '+PersianNumbers.toPersian((Formatter.f.format(int.parse(product_list[index].price)*product_list[index].quantity)).toString()),textDirection: TextDirection.rtl,textAlign: TextAlign.right),
                      ),
                    );
                  },itemCount: product_list.length,),

                ),
              ),

              GestureDetector(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  decoration: BoxDecoration(color: Colors.green),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('مبلغ کل : '+getTotal().toString(),style: TextStyle(color: Colors.white),),

                        Text('تایید و پرداخت',style: TextStyle(color: Colors.white),),



                      ],
                    ),
                  ),
                ),
                onTap: () {
                  if (token != null) {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => FinalCart(product_list)));
                  } else {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login(product_list)));
                  }
                }
                )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
//    Connection.checkConnection(context);
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == false){Navigator.push(context, MaterialPageRoute(builder: (context)=>noConnection()));}




    });

    product_list = widget.list;
    getToken();

  }
  String getTotal(){
    int total = 0;

    for(int i =0;i<product_list.length;i++){
      total+=int.parse(product_list[i].price)*product_list[i].quantity;
    }
    //return total.toString();
    return PersianNumbers.toPersian(Formatter.f.format(total)).toString();
  }
  addCartToShared(List list) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList('cart', list);
    sharedPreferences.commit();

  }
  getToken() async{
    token = await user_shared.getUserToken();
  }
}