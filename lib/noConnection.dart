import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class noConnection extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return noConnectionState();
  }

}
class noConnectionState extends State<noConnection>{
  @override
  var subscription;
  bool isConnected;
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.red
        ),
        child: Center(
          child: Text(
            'اتصال به اینترنت شما برقرار نمی باشد',
            style: TextStyle(color: Colors.yellowAccent),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {


      // Got a new connectivity status!
      if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){setState(() {
        isConnected = true;
      });}
      else{
        setState(() {
          isConnected = false;
        });

      }
      if(isConnected == true){Navigator.pop(context);}




    });

  }


}